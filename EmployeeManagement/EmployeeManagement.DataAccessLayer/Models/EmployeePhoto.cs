﻿using System.ComponentModel.DataAnnotations;
using EmployeeManagement.DataAccessLayer.Attributes;

namespace EmployeeManagement.DataAccessLayer.Models
{
    public class EmployeePhoto
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string PhotoPath { get; set; }
    }
}
