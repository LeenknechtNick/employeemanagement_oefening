﻿using System.Collections.Generic;
using System.Linq;
using EmployeeManagement.DataAccessLayer.Context;
using EmployeeManagement.DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.DataAccessLayer.Repositories
{
    public class EmployeePhotoRepository : IEmployeePhotoRepository
    {
        private readonly AppDbContext _context;

        public EmployeePhotoRepository(AppDbContext context)
        {
            _context = context;
        }

        public EmployeePhoto Add(EmployeePhoto employeePhoto)
        {
            var newEmployeePhoto = _context.EmployeePhotos.Add(employeePhoto);

            if (newEmployeePhoto != null && newEmployeePhoto.State == EntityState.Added)
            {
                var affectedRows = _context.SaveChanges();

                if (affectedRows > 0)
                {
                    return newEmployeePhoto.Entity;
                }
            }
            return null;
        }

        public EmployeePhoto Delete(int id)
        {
            var employeePhoto = _context.EmployeePhotos.Find(id);
            
            if(employeePhoto != null)
            {
                var deletedEmployeePhoto = _context.Remove(employeePhoto);

                if (deletedEmployeePhoto != null && deletedEmployeePhoto.State == EntityState.Deleted)
                {
                    var affectedRows = _context.SaveChanges();

                    if (affectedRows > 0)
                    {
                        return deletedEmployeePhoto.Entity;
                    }
                }
            }
            return null;
        }

        public IEnumerable<EmployeePhoto> GetAll()
        {
            var employeePhotos = _context.EmployeePhotos;
            return employeePhotos;
        }

        public IEnumerable<EmployeePhoto> GetAllById(int id)
        {
            List<EmployeePhoto> listEmployeePhotos = new List<EmployeePhoto>(_context.EmployeePhotos);
            List<EmployeePhoto> listEmployeePhotosById = new List<EmployeePhoto>();
            foreach (var photo in listEmployeePhotos)
            {
                if (photo.EmployeeId == id)
                {
                    listEmployeePhotosById.Add(photo);
                }
            }
            if (listEmployeePhotosById != null && listEmployeePhotosById.Count > 0)
            {
                return listEmployeePhotosById;
            }
            return null;
        }

        public EmployeePhoto Update(EmployeePhoto employeePhoto)
        {
            var newEmployeePhoto = _context.EmployeePhotos.Update(employeePhoto);

            if (newEmployeePhoto != null && newEmployeePhoto.State == EntityState.Modified)
            {
                var affectedRows = _context.SaveChanges();

                if (affectedRows > 0)
                {
                    return newEmployeePhoto.Entity;
                }
            }
            return null;
        }
    }
}
