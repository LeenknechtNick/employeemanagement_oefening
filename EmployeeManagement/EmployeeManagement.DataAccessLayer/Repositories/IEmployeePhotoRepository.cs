﻿using System.Collections.Generic;
using EmployeeManagement.DataAccessLayer.Models;

namespace EmployeeManagement.DataAccessLayer.Repositories
{
    public interface IEmployeePhotoRepository
    {
        IEnumerable<EmployeePhoto> GetAll();
        IEnumerable<EmployeePhoto> GetAllById(int id);
        EmployeePhoto Add(EmployeePhoto employeePhoto);
        EmployeePhoto Update(EmployeePhoto employeePhoto);
        EmployeePhoto Delete(int id);
    }
}
