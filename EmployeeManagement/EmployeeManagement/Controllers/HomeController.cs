﻿using System;
using System.Collections.Generic;
using System.IO;
using EmployeeManagement.DataAccessLayer.Models;
using EmployeeManagement.DataAccessLayer.Repositories;
using EmployeeManagement.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeManagement.Controllers
{
    //[Route("[controller]/[action]")]
    public class HomeController : Controller
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeePhotoRepository _employeePhotoRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public HomeController(
            IEmployeeRepository employeeRepository, 
            IEmployeePhotoRepository employeePhotoRepository,
            IWebHostEnvironment webHostEnvironment)
        {
            _employeeRepository = employeeRepository;
            _employeePhotoRepository = employeePhotoRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        //[Route("~/Home")]
        //[Route("~/")]
        public IActionResult Index()
        {
            //var employeeList = _employeeRepository.GetAll();
            //return View(employeeList);
            HomeIndexViewModel homeIndexViewModel = new HomeIndexViewModel()
            {
                Employees = _employeeRepository.GetAll(),
                EmployeePhotos = _employeePhotoRepository.GetAll(),
                PageTitle = "Employee Index"
            };
            return View(homeIndexViewModel);
        }

        //[Route("{id?}")]
        public IActionResult Details(int? id)
        {
            HomeDetailsViewModel homeDetailsViewModel = new HomeDetailsViewModel()
            {
                Employee = _employeeRepository.GetById(id ?? 1),
                EmployeePhotos = _employeePhotoRepository.GetAllById(id ?? 1),
                PageTitle = "Employee Details"
            };

            return View(homeDetailsViewModel);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(EmployeeCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                string uniqueFileName = null;
                List<string> uniqueFileNames = new List<string>();

                if (model.Photos != null && model.Photos.Count > 3)
                {
                    foreach (var photo in model.Photos)
                    {
                        var uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
                        uniqueFileName = $"{Guid.NewGuid().ToString()}_{photo.FileName}";
                        var filePath = Path.Combine(uploadsFolder, uniqueFileName);
                        photo.CopyTo(new FileStream(filePath, FileMode.Create));
                        uniqueFileNames.Add(uniqueFileName);
                    }
                }

                var newEmployee = new Employee
                {
                    Name = model.Name,
                    Email = model.Email,
                    Department = model.Department,
                    BankAccountNumber = model.BankAccountNumber
                    //PhotoPaths = uniqueFileNames
                };

                var response = _employeeRepository.Add(newEmployee);

                if (response != null && response.Id != 0)
                {
                    return RedirectToAction("Details", new { id = response.Id });
                }
            }

            return View();
        }
    }
}
