﻿using EmployeeManagement.DataAccessLayer.Models;
using System.Collections.Generic;

namespace EmployeeManagement.ViewModels
{
    public class HomeDetailsViewModel
    {
        public Employee Employee { get; set; }
        public IEnumerable<EmployeePhoto> EmployeePhotos { get; set; }
        public string PageTitle { get; set; }
    }
}
