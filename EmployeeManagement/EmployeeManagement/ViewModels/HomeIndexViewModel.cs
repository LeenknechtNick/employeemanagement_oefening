﻿using EmployeeManagement.DataAccessLayer.Models;
using System.Collections.Generic;

namespace EmployeeManagement.ViewModels
{
    public class HomeIndexViewModel
    {
        public IEnumerable<Employee> Employees { get; set; }
        public IEnumerable<EmployeePhoto> EmployeePhotos { get; set; }
        public string PageTitle { get; set; }
    }
}
